-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-03-2019 a las 06:55:59
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultaAutomovilDisponible`(ini date, fin date)
begin
select * from crud_automovil where matricula not in(select idAuto_id from crud_reserva r where 
ini between fechaIni and fechaFin
or
fin between fechaIni and fechaFin
or 
fechaIni >= ini and fechaFin <=fin
and
r.estado = '1');
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultaAutomovilDisponible2`(ini date, fin date)
begin
select idAuto_id from crud_reserva r where 
ini between fechaIni and fechaFin
or
fin between fechaIni and fechaFin
or 
fechaIni >= ini and fechaFin <=fin
and
r.estado = '1';
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add user', 3, 'add_user'),
(8, 'Can change user', 3, 'change_user'),
(9, 'Can delete user', 3, 'delete_user'),
(10, 'Can add group', 4, 'add_group'),
(11, 'Can change group', 4, 'change_group'),
(12, 'Can delete group', 4, 'delete_group'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add cliente', 7, 'add_cliente'),
(20, 'Can change cliente', 7, 'change_cliente'),
(21, 'Can delete cliente', 7, 'delete_cliente'),
(22, 'Can add automovil', 8, 'add_automovil'),
(23, 'Can change automovil', 8, 'change_automovil'),
(24, 'Can delete automovil', 8, 'delete_automovil'),
(25, 'Can add reserva', 9, 'add_reserva'),
(26, 'Can change reserva', 9, 'change_reserva'),
(27, 'Can delete reserva', 9, 'delete_reserva'),
(28, 'Can add datos usu', 10, 'add_datosusu'),
(29, 'Can change datos usu', 10, 'change_datosusu'),
(30, 'Can delete datos usu', 10, 'delete_datosusu'),
(31, 'Can add usu', 11, 'add_usu'),
(32, 'Can change usu', 11, 'change_usu'),
(33, 'Can delete usu', 11, 'delete_usu'),
(34, 'Can add recepcion reserva', 12, 'add_recepcionreserva'),
(35, 'Can change recepcion reserva', 12, 'change_recepcionreserva'),
(36, 'Can delete recepcion reserva', 12, 'delete_recepcionreserva'),
(37, 'Can add recepcionista', 13, 'add_recepcionista'),
(38, 'Can change recepcionista', 13, 'change_recepcionista'),
(39, 'Can delete recepcionista', 13, 'delete_recepcionista');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crud_automovil`
--

CREATE TABLE IF NOT EXISTS `crud_automovil` (
  `matricula` varchar(7) NOT NULL,
  `modelo` varchar(45) NOT NULL,
  `anno` int(11) NOT NULL,
  `precio` double NOT NULL,
  `foto` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `crud_automovil`
--

INSERT INTO `crud_automovil` (`matricula`, `modelo`, `anno`, `precio`, `foto`) VALUES
('ABC-931', 'sony 3', 2017, 300, 'img/amarillo.jpg'),
('ABC-A32', 'pun racer', 2007, 400, 'img/blanco.jpg'),
('CC1-2AA', 'orion 45', 2001, 100, 'img/negro.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crud_cliente`
--

CREATE TABLE IF NOT EXISTS `crud_cliente` (
  `licencia` varchar(9) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `crud_cliente`
--

INSERT INTO `crud_cliente` (`licencia`, `nombre`, `email`, `telefono`, `password`) VALUES
('G71962605', '', '', '', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crud_datosusu`
--

CREATE TABLE IF NOT EXISTS `crud_datosusu` (
  `id` int(11) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `ape` varchar(45) NOT NULL,
  `nac` date NOT NULL,
  `sexo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crud_recepcionista`
--

CREATE TABLE IF NOT EXISTS `crud_recepcionista` (
  `dni` varchar(8) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crud_recepcionreserva`
--

CREATE TABLE IF NOT EXISTS `crud_recepcionreserva` (
  `id` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `idRecepcionista_id` varchar(8) NOT NULL,
  `idReserva_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crud_reserva`
--

CREATE TABLE IF NOT EXISTS `crud_reserva` (
  `id` int(11) NOT NULL,
  `fechaIni` date NOT NULL,
  `fechaFin` date NOT NULL,
  `estado` int(11) NOT NULL,
  `idAuto_id` varchar(7) NOT NULL,
  `idCliente_id` varchar(9) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `crud_reserva`
--

INSERT INTO `crud_reserva` (`id`, `fechaIni`, `fechaFin`, `estado`, `idAuto_id`, `idCliente_id`) VALUES
(3, '2018-05-30', '2018-09-02', 1, 'ABC-931', 'G71962605'),
(4, '2017-01-01', '2017-02-02', 1, 'ABC-931', 'G71962605');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `crud_usu`
--

CREATE TABLE IF NOT EXISTS `crud_usu` (
  `id` int(11) NOT NULL,
  `usu` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(4, 'auth', 'group'),
(2, 'auth', 'permission'),
(3, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(8, 'crud', 'automovil'),
(7, 'crud', 'cliente'),
(10, 'crud', 'datosusu'),
(13, 'crud', 'recepcionista'),
(12, 'crud', 'recepcionreserva'),
(9, 'crud', 'reserva'),
(11, 'crud', 'usu'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-03-24 15:28:06.192000'),
(2, 'auth', '0001_initial', '2019-03-24 15:28:15.024000'),
(3, 'admin', '0001_initial', '2019-03-24 15:28:17.598000'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-03-24 15:28:17.754000'),
(5, 'contenttypes', '0002_remove_content_type_name', '2019-03-24 15:28:19.082000'),
(6, 'auth', '0002_alter_permission_name_max_length', '2019-03-24 15:28:20.190000'),
(7, 'auth', '0003_alter_user_email_max_length', '2019-03-24 15:28:21.079000'),
(8, 'auth', '0004_alter_user_username_opts', '2019-03-24 15:28:21.219000'),
(9, 'auth', '0005_alter_user_last_login_null', '2019-03-24 15:28:22.951000'),
(10, 'auth', '0006_require_contenttypes_0002', '2019-03-24 15:28:23.013000'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2019-03-24 15:28:23.122000'),
(12, 'auth', '0008_alter_user_username_max_length', '2019-03-24 15:28:23.918000'),
(13, 'crud', '0001_initial', '2019-03-24 15:28:24.417000'),
(14, 'crud', '0002_datosusu', '2019-03-24 15:28:24.823000'),
(15, 'crud', '0003_auto_20190324_1023', '2019-03-24 15:28:27.423000'),
(16, 'sessions', '0001_initial', '2019-03-24 15:28:28.067000'),
(17, 'crud', '0004_recepcionista_recepcionreserva', '2019-03-30 05:55:28.876000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`);

--
-- Indices de la tabla `crud_automovil`
--
ALTER TABLE `crud_automovil`
  ADD PRIMARY KEY (`matricula`);

--
-- Indices de la tabla `crud_cliente`
--
ALTER TABLE `crud_cliente`
  ADD PRIMARY KEY (`licencia`);

--
-- Indices de la tabla `crud_datosusu`
--
ALTER TABLE `crud_datosusu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `crud_recepcionista`
--
ALTER TABLE `crud_recepcionista`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `crud_recepcionreserva`
--
ALTER TABLE `crud_recepcionreserva`
  ADD PRIMARY KEY (`id`),
  ADD KEY `crud_recep_idRecepcionista_id_d5ff1260_fk_crud_recepcionista_dni` (`idRecepcionista_id`),
  ADD KEY `crud_recepcionreserva_idReserva_id_9e62d83a_fk_crud_reserva_id` (`idReserva_id`);

--
-- Indices de la tabla `crud_reserva`
--
ALTER TABLE `crud_reserva`
  ADD PRIMARY KEY (`id`),
  ADD KEY `crud_reserva_idAuto_id_3474adfd_fk_crud_automovil_matricula` (`idAuto_id`),
  ADD KEY `crud_reserva_idCliente_id_eb71a73c_fk_crud_cliente_licencia` (`idCliente_id`);

--
-- Indices de la tabla `crud_usu`
--
ALTER TABLE `crud_usu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_de54fa62` (`expire_date`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `crud_datosusu`
--
ALTER TABLE `crud_datosusu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `crud_recepcionreserva`
--
ALTER TABLE `crud_recepcionreserva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `crud_reserva`
--
ALTER TABLE `crud_reserva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `crud_usu`
--
ALTER TABLE `crud_usu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `crud_recepcionreserva`
--
ALTER TABLE `crud_recepcionreserva`
  ADD CONSTRAINT `crud_recep_idRecepcionista_id_d5ff1260_fk_crud_recepcionista_dni` FOREIGN KEY (`idRecepcionista_id`) REFERENCES `crud_recepcionista` (`dni`),
  ADD CONSTRAINT `crud_recepcionreserva_idReserva_id_9e62d83a_fk_crud_reserva_id` FOREIGN KEY (`idReserva_id`) REFERENCES `crud_reserva` (`id`);

--
-- Filtros para la tabla `crud_reserva`
--
ALTER TABLE `crud_reserva`
  ADD CONSTRAINT `crud_reserva_idAuto_id_3474adfd_fk_crud_automovil_matricula` FOREIGN KEY (`idAuto_id`) REFERENCES `crud_automovil` (`matricula`),
  ADD CONSTRAINT `crud_reserva_idCliente_id_eb71a73c_fk_crud_cliente_licencia` FOREIGN KEY (`idCliente_id`) REFERENCES `crud_cliente` (`licencia`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

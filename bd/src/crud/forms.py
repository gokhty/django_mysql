from django import forms
from .models import Usu, Reserva, Cfechas
from .models import DatosUsu

BIRTH_YEAR_CHOICES = ('1980', '1981', '1982')
class UsuForm(forms.ModelForm):
	clave = forms.CharField(widget=forms.PasswordInput)
	class Meta:
		model = Usu
		fields = ["usu", "clave"]

class DatosUsuForm(forms.ModelForm):
	class Meta:
		model = DatosUsu
		fields = ["nom", "ape","nac", "sexo"]

class ReservaForm(forms.ModelForm):
	desde = forms.CharField(widget=forms.TextInput(attrs={'class': 'datepicker'}), label="Desde")
	hasta = forms.CharField(widget=forms.TextInput(attrs={'class': 'datepicker'}), label="Hasta")
	class Meta:
		model = Cfechas
		fields = ["desde","hasta"]

class RegReservaForm(forms.ModelForm):
	class Meta:
		model = Reserva
		fields = ["idCliente", "idAuto","fechaIni", "fechaFin", "estado"]

from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Usu(models.Model):
	id = models.AutoField(primary_key=True)
	usu = models.CharField(max_length=45)
	clave = models.CharField(max_length=45)

class DatosUsu(models.Model):
	id = models.AutoField(primary_key=True)
	nom = models.CharField(max_length=45)
	ape = models.CharField(max_length=45)
	nac = models.DateField()
	sexo = models.IntegerField()

class Cliente(models.Model):
	licencia = models.CharField(max_length=9, primary_key=True)
	nombre = models.CharField(max_length=45)
	email = models.CharField(max_length=45)
	telefono = models.CharField(max_length=45)
	password = models.CharField(max_length=45)

class Automovil(models.Model):
	matricula = models.CharField(max_length=7, primary_key=True)
	modelo = models.CharField(max_length=45)
	anno = models.IntegerField()
	precio = models.FloatField()
	foto = models.CharField(max_length=45)

class Reserva(models.Model):
	id = models.AutoField(primary_key=True)
	idCliente = models.ForeignKey(Cliente, on_delete = models.CASCADE)
	idAuto = models.ForeignKey(Automovil, on_delete = models.CASCADE)
	fechaIni = models.DateField()
	fechaFin = models.DateField()
	estado = models.IntegerField()

class Recepcionista(models.Model):
	dni = models.CharField(max_length=8, primary_key=True)
	nombre = models.CharField(max_length=45)
	password = models.CharField(max_length=45)

class RecepcionReserva(models.Model):
	id = models.AutoField(primary_key=True)
	idReserva = models.ForeignKey(Reserva, on_delete = models.CASCADE)
	idRecepcionista = models.ForeignKey(Recepcionista, on_delete = models.CASCADE)
	estado = models.IntegerField()

class Cfechas(models.Model):
	desde = models.DateField()
	hasta = models.DateField()
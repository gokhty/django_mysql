import pymysql.cursors
from .entidades import ClsDatosUsuario
# https://docs.djangoproject.com/en/2.1/topics/db/sql/
# https://pymysql.readthedocs.io/en/latest/user/examples.html

class Datos(object):

	listDatosUsuario = []

	def listaUsuario(self):
		connection = pymysql.connect(host='localhost',
	                             user='root',
	                             password='',
	                             db='bd')
		
		try:
		    with connection.cursor() as cursor:
		        # Read a single record
		        sql = "select * from crud_datosusu"
		        cursor.execute(sql)
		        result = cursor.fetchall()

		        for aaa in self.listDatosUsuario:
			    		self.listDatosUsuario.remove(aaa)
			    		
		        for i in result:
			        self.listDatosUsuario.append(
			        	ClsDatosUsuario(
			        	i[0],
			        	i[1],
			        	i[2],
			        	i[3],
			        	i[4]))
			        

		finally:
		    connection.close()


	def insertaDatosUsu(self, a, b, c, d):
		connection = pymysql.connect(host='localhost',
	                             user='root',
	                             password='',
	                             db='bd')
		try:
		    with connection.cursor() as cursor:
		        # Read a single record
		        sql = "insert into crud_datosUsu(nom, ape, nac, sexo)values(%s,%s,%s,%s)"
		        cursor.execute(sql, (a, b, c, d))	

		    connection.commit()

		finally:
		    connection.close()

	def insertaUsu(self, a, b):
		connection = pymysql.connect(host='localhost',
	                             user='root',
	                             password='',
	                             db='bd')
		try:
		    with connection.cursor() as cursor:
		        # Read a single record
		        sql = "insert into crud_usu(usu,clave)values(%s,%s)"
		        cursor.execute(sql, (a, b))

		    connection.commit()

		finally:
		    connection.close()

	def insertaReserva(self, a, b,c,d,e):
		connection = pymysql.connect(host='localhost',
	                             user='root',
	                             password='',
	                             db='bd')
		try:
		    with connection.cursor() as cursor:
		        # Read a single record
		        sql = "insert into crud_reserva(idAuto_id, idCliente_id, fechaIni, fechaFin, estado)values(%s,%s,%s,%s,%s)"
		        cursor.execute(sql, (a, b,c,d,e))

		    connection.commit()

		finally:
		    connection.close()
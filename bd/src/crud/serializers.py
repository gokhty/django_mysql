from rest_framework import serializers
from .models import Reserva, Automovil

class ReservaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Reserva
		fields = ["idCliente", "idAuto","fechaIni", "fechaFin", "estado"]

class AutomovilSerializer(serializers.ModelSerializer):
	class Meta:
		model = Automovil
		fields = ["matricula", "modelo","anno", "precio", "foto"]
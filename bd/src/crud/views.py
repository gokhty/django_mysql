from django.shortcuts import render
from .forms import UsuForm
from .forms import DatosUsuForm, ReservaForm, RegReservaForm
from .models import Usu, Automovil, Reserva
from .models import DatosUsu
from django.http import HttpResponseRedirect, HttpResponse
from django.core import serializers
import json
from .datos import Datos
from datetime import datetime
from django.db.models import Q
from django.http import JsonResponse
from django.views.generic import TemplateView
from django.shortcuts import redirect
from .serializers import ReservaSerializer, AutomovilSerializer
from rest_framework import generics
from rest_framework.decorators import api_view

# Create your views here.

my_list = []
autoNoReservado = []
fechas = []

def cuenta(request):
	
	form = UsuForm(request.POST or None)
	# print(dir(form))
	context = {
		"form" : form,
	}

	if(form.is_valid()):
		instance = form.save(commit=False)
		usu = form.cleaned_data.get("usu")
		clave = form.cleaned_data.get("clave")

		#print form.cleaned_data
		#a = form.cleaned_data
		#print a.get("nombre")
		#print a.get("edad")

		"""db = Usu(usu = usu, clave = clave)
		db.save();

		global my_list

		db1 = DatosUsu(nom = my_list[0], ape = my_list[1], nac = my_list[2], sexo = my_list[3])
		db1.save();"""

		dts = Datos()
		dts.insertaUsu(usu, clave)
		global my_list
		dts.insertaDatosUsu(my_list[0],my_list[1],my_list[2],my_list[3])

		return HttpResponseRedirect('/')

	return render(request, "inicio.html", context)

def datos(request):
	
	form = DatosUsuForm(request.POST or None)
	# print(dir(form))
	context = {
		"form" : form,
	}

	if(form.is_valid()):
		instance = form.save(commit=False)
		nom = form.cleaned_data.get("nom")
		ape = form.cleaned_data.get("ape")
		nac = form.cleaned_data.get("nac")
		sexo = form.cleaned_data.get("sexo")

		print form.cleaned_data
		a = form.cleaned_data
		print a.get("nom")
		print a.get("ape")
		print a.get("sexo")
		#db = DatosUsu(nom = nom, ape = ape, nac = nac, sexo = sexo)
		#db.save();

		global my_list
		my_list = [nom, ape, nac, sexo]

		return HttpResponseRedirect('/cuenta')

	return render(request, "datos.html", context)

def mostrar(request):
	"""
		id__gt = 3
		id > 3
		get >=	|	lt <	|	lte <=
	"""
	form = ReservaForm(request.POST or None)
	if request.method == "POST":
		if(form.is_valid()):
			instance = form.save(commit=False)
			ini = form.cleaned_data.get("desde")
			fin = form.cleaned_data.get("hasta")

			print '----------------------------'
			
			fechaIni = ini
			fechaFin = fin



			autoReservado = Reserva.objects.filter(Q(Q(fechaIni__gte=fechaIni) & Q(fechaIni__lte=fechaFin)) | Q(Q(fechaFin__gte=fechaIni) & Q(fechaFin__lte=fechaFin)) | Q(Q(fechaIni__gte=fechaIni) & Q(fechaFin__lte=fechaFin)) | Q(Q(fechaIni__lte=fechaIni) & Q(fechaFin__gte=fechaFin))).values('idAuto_id')
			
			#Reserva.objects.filter(fechaIni__gte=fechaIni).filter(fechaIni__gte=fechaIni)
			# abc = serializers.serialize("json",DatosUsu.objects.all(),fields=('nom','ape','sexo'))
			# print(abc)
			global autoNoReservado
			autoNoReservado = Automovil.objects.exclude(matricula__in=autoReservado)
			#hola = Automovil.objects.exclude(matricula__in=autoReservado)

			hola = serializers.serialize("json",Automovil.objects.exclude(matricula__in=autoReservado),fields=('modelo','matricula','precio','anno','foto'))

			global fechas
			fechas = [fechaIni, fechaFin]
			print fechas[0]
			print fechas[1]
			print hola
	
		return JsonResponse(
	            {
	                'content': {
	                    'message': hola,
	                }
	            }
	        )
	else:
		context = {
			"form" : form,
			"titulo" : autoNoReservado,
		}
		return render(request, "mostrar.html", context )

    #return HttpResponse(json.dumps({'message': 'por json'}), content_type='application/json')
	#return render(request, "mostrar.html", context )
	


"""
def mostrar(request):

	dts = Datos()
	dts.listaUsuario()
	return render(request, "mostrar.html", { "titulo" : dts.listDatosUsuario })
"""	

def reservar(request):
	form = RegReservaForm(request.POST or None)

	context = {
		"a" : "a",
	}

	if(form.is_valid()):

		# idCliente idAuto fecahIni fechaFin estado
		autom = form.cleaned_data.get("idAuto")
		print autom
		db = Reserva(idCliente = 1, idAuto = autom, fechaIni = fechas[0], fechaFin = fechas[1], estado = "1")
		db.save();
		return HttpResponseRedirect('/')

		return render(request, "mostrar.html", context )
		

class ReservaLista(generics.ListAPIView):
	queryset = Reserva.objects.all()
	serializer_class = ReservaSerializer

class ReservaDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Reserva.objects.all()
	serializer_class = ReservaSerializer

class AutomovilLista(generics.ListCreateAPIView):
	serializer_class = AutomovilSerializer

	def get_queryset(self):

		print "en rest"  
		
		queryset = Automovil.objects.all()
		autoReservado1 = Reserva.objects.all()

		desde = self.request.query_params.get('desde', None)
		hasta = self.request.query_params.get('hasta', None)
		print desde
		print hasta

		if hasta is not None:
			

			autoReservado1 = autoReservado1.filter(Q(Q(fechaIni__gte=desde) & Q(fechaIni__lte=hasta)) | Q(Q(fechaFin__gte=desde) & Q(fechaFin__lte=hasta)) | Q(Q(fechaIni__gte=desde) & Q(fechaFin__lte=hasta)) | Q(Q(fechaIni__lte=desde) & Q(fechaFin__gte=hasta))).values('idAuto_id')
			queryset = queryset.exclude(matricula__in=autoReservado1)
		return queryset

class AutomovilDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = Automovil.objects.all()
	serializer_class = AutomovilSerializer
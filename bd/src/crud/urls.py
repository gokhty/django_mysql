from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from crud import views

urlpatterns = [
	url(r'^automovil/$', views.AutomovilLista.as_view()),
	url(r'^automovil/$', views.AutomovilDetail.as_view()),
	url(r'^reserva/$', views.ReservaLista.as_view()),
	url(r'^reserva/$', views.ReservaDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
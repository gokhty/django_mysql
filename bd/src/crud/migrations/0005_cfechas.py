# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2019-04-02 15:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crud', '0004_recepcionista_recepcionreserva'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cfechas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desde', models.DateField()),
                ('hasta', models.DateField()),
            ],
        ),
    ]

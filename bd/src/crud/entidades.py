class ClsDatosUsuario(object):
    def __init__(self, id=None, nom=None, ape=None, nac=None, sexo=None):
        self.id = id
        self.nom = nom
        self.ape = ape
        self.nac = nac
        self.sexo = sexo

class ClsUsuario(object):
    def __init__(self, id=None, usu=None, clave=None):
        self.id = id
        self.usu = usu
        self.clave = clave

class ClsReserva(object):
    def __init__(self, idCliente=None, idAuto=None, fechaIni=None, fechaFin=None, estado=None):
        self.idCliente = idCliente
        self.idAuto = idAuto
        self.fechaIni = fechaIni
        self.fechaFin = fechaFin
        self.estado = estado